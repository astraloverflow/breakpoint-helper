# Breakpoint-Helper

[![License][license-img]](https://github.com/astraloverflow/breakpoint-helper/blob/master/LICENSE)
[![Version][version-img]](https://github.com/astraloverflow/breakpoint-helper/releases)
[![Last Commit][last-commit-img]](https://github.com/astraloverflow/breakpoint-helper/commits/master)
[![Open Issues][issues-img]](https://github.com/astraloverflow/breakpoint-helper/issues)

> A utility for working with Bootstrap's media query breakpoints

## Installation

### NPM
```shell
$ npm install -D astraloverflow/breakpoint-helper
```

### Yarn
```shell
$ yarn add astraloverflow/breakpoint-helper -D
```

## Setup

### Option 1

Import `breakpoint-helper.css` into your HTML file.

```html
<link rel="stylesheet" href="./node_modules/breakpoint-helper/breakpoint-helper.css" />
```

### Option 2

Import `_breakpoint-helper.scss` into your project's sass/scss files.

```scss
@import "./node_modules/breakpoint-helper/breakpoint-helper";
```

**Important:** Requires that you also import Bootstrap's scss files as `_breakpoint-helper.scss` uses several of Bootstrap's variables (see `build.scss`).

## Usage

Add the class `bootstrap-breakpoints` to the `<body>` tag of your HTML file.

```html
<body class="bootstrap-breakpoints">
```

You will now see a floating purple rectangle telling you what the current Bootstrap breakpoint is.

![A animated GIF previewing the effect of this tool](./preview.gif)

[license-img]: https://img.shields.io/github/license/astraloverflow/breakpoint-helper.svg
[version-img]: https://img.shields.io/github/release/astraloverflow/breakpoint-helper.svg
[last-commit-img]: https://img.shields.io/github/last-commit/astraloverflow/breakpoint-helper.svg
[issues-img]: https://img.shields.io/github/issues-raw/astraloverflow/breakpoint-helper.svg
